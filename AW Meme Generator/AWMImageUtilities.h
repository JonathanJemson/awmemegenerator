//
//  AWMImageUtilities.h
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AWMImageUtilities : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic, readonly) UIViewController *parentViewController;

/**
 *  Creates an instance of the utilities to be used a specified
 *  parent view controller.
 *
 *  @param parentViewController The view controller that will present the image pickers
 *
 *  @return An instance of AWMImageUtilities.
 */
- (instancetype)initWithParentViewController:(UIViewController*)parentViewController;

/**
 *  Allows you to select an image from the user's photo library.
 *
 *  @param completionHandler A block of code to execute when the image has been selected.  The image is returned in the arguments.
 */
- (void)presentImagePickerWithCompletionHandler:(void (^)(UIImage*))completionHandler;

/**
 *  Allows the user to take a picture using the camera.
 *
 *  @param completionHandler A block of code to execute when the picture has been taken.  The picture is returned in the arguments.
 *
 *  @return Returns NO if the camera cannot be used, otherwise YES.
 */
- (BOOL)presentCameraWithCompletionHandler:(void (^)(UIImage*))completionHandler;

/**
 *  Checks if the camera is available for use.
 *
 *  @return YES if the camera is available for use.  NO if there is no camera or it is disabled.
 */
- (BOOL)canUseCamera;

@end
