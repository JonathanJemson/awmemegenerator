//
//  AWMViewController.m
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import "AWMViewController.h"
#import "AWMImageUtilities.h"
@interface AWMViewController ()

@property (nonatomic) AWMImageUtilities *imageUtils;
@property (nonatomic, copy) void (^handleImageBlock)(UIImage*);

@end

@implementation AWMViewController

// Constants
static const NSTimeInterval kKeyboardShowAnimationDuration = 0.35;
static const NSTimeInterval kKeyboardHideAnimationDuration = 0.20;
static const int kKeyboardOffset = 3;
static const CGFloat kOrigin = 0;
static const NSUInteger kUpperTextFieldTag = 2;
static const NSUInteger kLowerTextFieldTag = 3;

#pragma mark - IBActions
//Helpers:
//

- (IBAction)sharePhoto:(id)sender
{
    UIImage *image = [self screenshotWithImageView:self.imageView andUpperLabel:self.upperLabel andLowerLabel:self.lowerLabel];
    
    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:@[image] applicationActivities:nil];
    
    [self presentViewController:activityView animated:YES completion:nil];
}

- (IBAction)accessPhotos:(id)sender
{
    [self.imageUtils presentImagePickerWithCompletionHandler:self.handleImageBlock];
}

- (IBAction)accessCamera:(id)sender
{
    if ([self.imageUtils canUseCamera])
    {
        [self.imageUtils presentCameraWithCompletionHandler:self.handleImageBlock];
    }
    else
    {
        [self showAlertWithTitle:@"Camera" withMessage:@"Cannot currently use camera."];
    }
}

- (IBAction)updateMemeText:(id)sender
{
    self.upperLabel.text = self.upperTextField.text;
    self.lowerLabel.text = self.lowerTextField.text;
    
    [self applyStrokeToLabel:self.upperLabel];
    [self applyStrokeToLabel:self.lowerLabel];
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Prepare the Text Fields here (use the helper method)
    [self prepareTextField:self.upperTextField isLowerField:NO];
    [self prepareTextField:self.lowerTextField isLowerField:YES];
    
    // Set the labels initial text to the empty string.
    // Otherwise, Interface Builder will set the default strings
    self.lowerLabel.text = @"";
    self.upperLabel.text = @"";
}

- (AWMImageUtilities *)imageUtils
{
    if(!_imageUtils)
    {
        // Instantiate Image Utilites to handle the ImagePicker and Camera UI for you
        _imageUtils = [[AWMImageUtilities alloc] initWithParentViewController:self];
    }
    
    return _imageUtils;
}

// Because the images are sent back asynchronously,
// we have chosen to provide a block which receives the image
// and updates the views
- (void(^)(UIImage *)) handleImageBlock
{
    if (!_handleImageBlock)
    {
        // We must declare a weak reference to the image view
        // Otherwise, we will create a reference cycle and leak memory
        
        // ******* ASSIGN THE IMAGE VIEW HERE ***********
        
        __weak UIImageView* blockImageView = self.imageView;
        
        // ******* DON'T FORGET TO ASSIGN THIS **********
        
        _handleImageBlock = ^(UIImage *img)
        {
            // Content Mode depends on image orientation.
            if (img.size.width / img.size.height >= blockImageView.bounds.size.width / blockImageView.bounds.size.height)
            {
                //blockImageView.contentMode = UIViewContentModeScaleAspectFit;
                blockImageView.contentMode = UIViewContentModeScaleAspectFill;
            }
            else
            {
                blockImageView.contentMode = UIViewContentModeScaleAspectFit;
                //blockImageView.contentMode = UIViewContentModeScaleAspectFill;
            }
            
            blockImageView.image = img;
        };
    }
    return _handleImageBlock;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // iOS notifies us when the keyboard appears and disappears.
    // We can use this to handle sliding the view up and down.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // When this view is not visible, we don't want to observe
    // the notifications, so we unregister
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard handling
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect keyboard = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboard.size.height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:kKeyboardShowAnimationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    self.view.frame = CGRectMake(0,-(keyboardHeight - kKeyboardOffset),screenWidth,screenHeight);
    [UIView commitAnimations];

}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:kKeyboardHideAnimationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    self.view.frame = CGRectMake(0,0,screenWidth,screenHeight);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == kUpperTextFieldTag)
    {
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == kLowerTextFieldTag)
                [subview becomeFirstResponder];
        }
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Helpers

- (UIImage*)screenshotWithImageView:(UIImageView*)imageView andUpperLabel:(UILabel*)upperLabel andLowerLabel:(UILabel*)lowerLabel
{
    CGRect origTopBounds = upperLabel.bounds;
    CGRect origBottomBounds = lowerLabel.bounds;
    upperLabel.bounds = CGRectMake(kOrigin,
                                        kOrigin,
                                        imageView.bounds.size.width,
                                        upperLabel.bounds.size.height);
    lowerLabel.bounds = CGRectMake(kOrigin,
                                        imageView.bounds.size.height-lowerLabel.bounds.size.height,
                                        imageView.bounds.size.width,
                                        lowerLabel.bounds.size.height);
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 0.0);
    
    [imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
    [upperLabel.layer renderInContext:UIGraphicsGetCurrentContext()];
    [lowerLabel.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    upperLabel.bounds = origTopBounds;
    lowerLabel.bounds = origBottomBounds;
    return screenshot;
}

- (void)showAlertWithTitle:(NSString*)title withMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)applyStrokeToLabel:(UILabel*)label
{
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:label.text];
    label.text = @"";
    NSRange fullStringRange = NSMakeRange(0, attrText.length);
    [attrText addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:fullStringRange];
    [attrText addAttribute:NSStrokeColorAttributeName value:[UIColor blackColor] range:fullStringRange];
    [attrText addAttribute:NSStrokeWidthAttributeName value:@(-5.0) range:fullStringRange];
    [attrText addAttribute:NSFontAttributeName value:label.font range:fullStringRange];
    label.attributedText = attrText;
}

- (void)prepareTextField:(UITextField*)textField isLowerField:(BOOL)isLower
{
    // Text field setup
    textField.delegate = self;
    
    // The upper keyboard will say Next and the lower one will say Done.
    textField.returnKeyType = isLower ? UIReturnKeyDone : UIReturnKeyNext;
    
    textField.tag = isLower ? kLowerTextFieldTag : kUpperTextFieldTag;
    
    // Memes are usually all uppercase
    textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
}

@end
