//
//  AWMViewController.h
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWMViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *upperLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *upperTextField;
@property (weak, nonatomic) IBOutlet UITextField *lowerTextField;


@end
