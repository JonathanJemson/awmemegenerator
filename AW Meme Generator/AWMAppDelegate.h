//
//  AWMAppDelegate.h
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
