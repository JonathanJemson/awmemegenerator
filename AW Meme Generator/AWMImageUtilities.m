//
//  AWMImageUtilities.m
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import "AWMImageUtilities.h"

@interface AWMImageUtilities ()
@property (nonatomic, copy) void (^callback) (UIImage*) ;
@end

@implementation AWMImageUtilities
- (instancetype)initWithParentViewController:(UIViewController*)parentViewController
{
    self = [super init];
    if (self) {
        _parentViewController = parentViewController;
    }
    return self;
}
- (void)presentImagePickerWithCompletionHandler:(void (^)(UIImage*))completionHandler
{
    self.callback = completionHandler;
    UIImagePickerController *picker = [self imagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self.parentViewController presentViewController:picker animated:YES completion:nil];
}
- (BOOL)presentCameraWithCompletionHandler:(void (^)(UIImage*))completionHandler
{
    // Ensure device has can use the camera
    if (![self canUseCamera]) {
        return NO;
    }
    self.callback = completionHandler;
    UIImagePickerController *picker = [self imagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    [self.parentViewController presentViewController:picker animated:YES completion:nil];
    return YES;
}

- (BOOL)canUseCamera
{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (UIImagePickerController *)imagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = NO;
    imagePicker.sourceType = sourceType;
    imagePicker.delegate = self;
    imagePicker.mediaTypes = @[(NSString*)kUTTypeImage];
    return imagePicker;
}

#pragma mark - Image Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* selectedImage = nil;
    selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if(!selectedImage)
    {
        selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    // I suppose the callback could be nil
    if (self.callback)
    {
        self.callback(selectedImage);
        self.callback = nil;
    }
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
