//
//  main.m
//  AW Meme Generator
//
//  Created by Jonathan Jemson on 6/12/14.
//  Copyright (c) 2014 AirWatch, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AWMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AWMAppDelegate class]));
    }
}
